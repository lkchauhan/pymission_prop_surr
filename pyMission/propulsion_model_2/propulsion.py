"""
INTENDED FOR MISSION ANALYSIS USE
provides propulsion models for the use of mission analysis.
The mission analysis and trajectory optimization tool was developed by:
    Jason Kao*
    John Hwang*

* University of Michigan Department of Aerospace Engineering,
  Multidisciplinary Design Optimization lab
  mdolab.engin.umich.edu

copyright July 2014
"""

# pylint: disable=E1101
from __future__ import division
import sys
from framework import *
import numpy

def setup_prop_surr(surr_file):

    raw = numpy.loadtxt(surr_file+'_inputs.dat')
    [Thrust, TSFC] = numpy.loadtxt(surr_file+'_outputs.dat')

    M_num, h_num, t_num = raw[:3].astype(int)
    M_surr = raw[3:3 + M_num]
    h_surr = raw[3 + M_num:3 + M_num + h_num]
    t_surr = raw[3 + M_num + h_num:]

    mbi_Thrust = numpy.zeros((M_num, h_num, t_num))
    mbi_TSFC = numpy.zeros((M_num, h_num, t_num))

    count = 0
    for i in xrange(M_num):
        for j in xrange(h_num):
            for k in xrange(t_num):
                mbi_Thrust[i][j][k] = Thrust[count]
                mbi_TSFC[i][j][k] = TSFC[count]
                count += 1

    Thrust_arr = MBI.MBI(mbi_Thrust, [M_surr, h_surr, t_surr],
                         [M_num, h_num, t_num], [4, 4, 4])
    TSFC_arr = MBI.MBI(mbi_TSFC, [M_surr, h_surr, t_surr],
                       [M_num, h_num, t_num], [4, 4, 4])

    nums = {
        'M': M_num,
        'h': h_num,
        't': t_num,
        }

    return [Thrust_arr, TSFC_arr, nums]

class SysPropSurrogate(ImplicitSystem):

    def _declare(self):

        self.num_elem = self.kwargs['num_elem']
        self.num = self.kwargs['num']
        self.Thrust_arr = self.kwargs['Thrust']
        self.wing_area = self.kwargs['S']
        ind_pts = range(self.num_elem + 1)

        self._declare_variable('tau', size=self.num_elem+1)
        self._declare_argument('M', indices=ind_pts)
        self._declare_argument('h', indices=ind_pts)
        self._declare_argument('CT_tar', indices=ind_pts)
        self._declare_argument('rho', indices=ind_pts)
        self._declare_argument('v', indices=ind_pts)

        self.J_CT = [None for i in range(3)]
        self.CT = [None for i in range(3)]

    def apply_F(self):

        pvec = self.vec['p']
        uvec = self.vec['u']
        fvec = self.vec['f']

        M_num = self.num['M']
        h_num = self.num['h']
        t_num = self.num['t']

        tau = uvec('tau')
        Mach = pvec('M')
        alt = pvec('h') * 1e3
        CT_tar = pvec('CT_tar') * 1e-1
        rho = pvec('rho')
        speed = pvec('v') * 1e2
        wing_area = self.wing_area * 1e2
        tau_res = fvec('tau')

        inputs = numpy.zeros((self.num_elem+1, 3))
        inputs[:, 0] = Mach
        inputs[:, 1] = alt
        inputs[:, 2] = tau

        CT_temp = self.Thrust_arr.evaluate(inputs)

        CT = numpy.zeros(self.num_elem+1)
        for index in xrange(self.num_elem+1):
            CT[index] = CT_temp[index, 0]

        CT[:] = CT[:] / (0.5 * rho[:] * speed[:]**2 * wing_area)

        tau_res[:] = CT - CT_tar

    def linearize(self):

        pvec = self.vec['p']
        uvec = self.vec['u']

        M_num = self.num['M']
        h_num = self.num['h']
        t_num = self.num['t']

        tau = uvec('tau')
        Mach = pvec('M')
        alt = pvec('h') * 1e3

        inputs = numpy.zeros((self.num_elem+1, 3))
        inputs[:, 0] = Mach
        inputs[:, 1] = alt
        inputs[:, 2] = tau

        for index in xrange(3):
            self.J_CT[index] = self.Thrust_arr.evaluate(inputs,
                                                        1+index,
                                                        0)[:, 0]

        CT_temp = self.Thrust_arr.evaluate(inputs)
        for index in xrange(self.num_elem+1):
            self.CT[index] = CT_temp[index, 0]

    def apply_dFdpu(self, args):

        dpvec = self.vec['dp']
        duvec = self.vec['du']
        dfvec = self.vec['df']
        pvec = self.vec['p']

        rho = pvec('rho')
        speed = pvec('v') * 1e2
        wing_area = self.wing_area * 1e2

        dtau = duvec('tau')
        dMach = dpvec('M')
        dalt = dpvec('h')
        dCT = dpvec('CT_tar')
        drho = dpvec('rho')
        dspeed = dpvec('speed')

        dres = dfvec('tau')

        if self.mode == 'fwd':
            dres[:] = 0.0
            if self.get_id('M') in args:
                dres[:] += self.J_CT[0] / (0.5 * rho[:] * speed[:]**2 *
                                           wing_area) * dMach
            if self.get_id('h') in args:
                dres[:] += self.J_CT[1] / (0.5 * rho[:] * speed[:]**2 *
                                           wing_area) * dalt * 1e3
            if self.get_id('tau') in args:
                dres[:] += self.J_CT[2] / (0.5 * rho[:] * speed[:]**2 *
                                           wing_area) * dtau
            if self.get_id('CT_tar') in args:
                dres[:] -= dCT
            if self.get_id('rho') in args:
                dres[:] -= self.CT / (0.5 * rho[:]**2 * speed[:]**2 *
                                      wing_area) * drho
            if self.get_id('v') in args:
                dres[:] -= 2 * self.CT / (0.5 * rho[:] * speed[:]**3 *
                                          wing_area) * dspeed * 1e2
        elif self.mode == 'rev':
            dtau[:] = 0.0
            dMach[:] = 0.0
            dalt[:] = 0.0
            dCT[:] = 0.0
            drho[:] = 0.0
            dspeed[:] = 0.0
            if self.get_id('M') in args:
                dMach[:] += self.J_CT[0] / (0.5 * rho[:] * speed[:]**2 *
                                            wing_area) * dres
            if self.get_id('h') in args:
                dalt[:] += self.J_CT[1] / (0.5 * rho[:] * speed[:]**2 *
                                           wing_area) * dres * 1e3
            if self.get_id('tau') in args:
                dtau[:] += self.J_CT[2] / (0.5 * rho[:] * speed[:]**2 *
                                           wing_area) * dres
            if self.get_id('CT_tar') in args:
                dCT[:] -= dres
            if self.get_id('rho') in args:
                drho[:] -= self.CT / (0.5 * rho[:]**2 * speed[:]**2 *
                                       wing_area) * dres
            if self.get_id('v') in args:
                dspeed[:] -= 2* self.CT / (0.5 * rho[:] * speed[:]**3 *
                                           wing_area) * dres * 1e2

class SysSFC(ExplicitSystem):
    """ linear SFC model wrt altitude """

    def _declare(self):
        """ owned variable: SFC (specific fuel consumption)
            dependencies: h (altitude)
                          SFCSL (sea-level SFC value)
        """

        self.num_elem = self.kwargs['num_elem']
        self.SFCSL = self.kwargs['SFCSL']
        num_pts = self.num_elem+1
        ind_pts = range(num_pts)

        self._declare_variable('SFC', size=num_pts)
        self._declare_argument('h', indices=ind_pts)

    def apply_G(self):
        """ compute SFC value using sea level SFC and altitude
            the model is a linear correction for altitude changes
        """

        pvec = self.vec['p']
        uvec = self.vec['u']
        alt = pvec('h') * 1e3
        sfcsl = self.SFCSL * 1e-6
        sfc = uvec('SFC')

        sfc_temp = sfcsl + (6.39e-13*9.81) * alt
        sfc[:] = sfc_temp / 1e-6

    def apply_dGdp(self, args):
        """ compute SFC derivatives wrt sea level SFC and altitude """

        dpvec = self.vec['dp']
        dgvec = self.vec['dg']

        dalt = dpvec('h')
        dsfc = dgvec('SFC')

        dsfc_dalt = 6.39e-13 * 9.81

        if self.mode == 'fwd':
            dsfc[:] = 0.0
            if self.get_id('h') in args:
                dsfc[:] += (dsfc_dalt * dalt) * 1e3/1e-6

        if self.mode == 'rev':
            dalt[:] = 0.0

            if self.get_id('h') in args:
                dalt[:] += dsfc_dalt * dsfc * 1e3/1e-6

class SysTau(ExplicitSystem):
    """ throttle setting determined primarily by thrust coefficient
        A simple linear relationship using the sea-level max thrust
        and a linear dependence on altitude is used
    """

    def _declare(self):
        """ owned variable: tau (throttle setting)
            dependencies: CT (coefficient of thrust)
                          rho (density)
                          v (speed)
                          h (altitude)
                          thrust_sl (maximum sea-level thrust)
                          S (wing area)
        """

        self.num_elem = self.kwargs['num_elem']
        self.thrust_sl = self.kwargs['thrust_sl']
        self.wing_area = self.kwargs['S']
        num_pts = self.num_elem+1
        ind_pts = range(num_pts)

        self._declare_variable('tau', size=num_pts)
        self._declare_argument('CT_tar', indices=ind_pts)
        self._declare_argument('rho', indices=ind_pts)
        self._declare_argument('v', indices=ind_pts)
        self._declare_argument('h', indices=ind_pts)

    def apply_G(self):
        """ compute throttle setting primarily using thrust coefficient """

        pvec = self.vec['p']
        uvec = self.vec['u']

        thrust_c = pvec('CT_tar') * 1e-1
        rho = pvec('rho')
        speed = pvec('v') * 1e2
        alt = pvec('h') * 1e3
        thrust_sl = self.thrust_sl * 1e6
        wing_area = self.wing_area * 1e2
        tau = uvec('tau')

        cThrust = thrust_sl - 72/1e3 * alt
        Thrust = 0.5*rho*speed**2*wing_area*thrust_c
        tau[:] = (Thrust / cThrust)

    def linearize(self):
        """ pre-compute the throttle derivatives wrt density, velocity
            wing area, thrust coefficient, sea level thrust, and altitude
        """

        pvec = self.vec['p']

        thrust_sl = self.thrust_sl * 1e6
        wing_area = self.wing_area * 1e2
        alt = pvec('h') * 1e3
        thrust_c = pvec('CT_tar') * 1e-1
        rho = pvec('rho')
        speed = pvec('v') * 1e2

        self.dt_drho = ((0.5*speed**2*wing_area*thrust_c) / (thrust_sl-72/1e3*alt))
        self.dt_dspeed = ((rho*speed*wing_area*thrust_c) / (thrust_sl-72/1e3*alt))
        self.dt_dthrust_c = ((0.5*rho*speed**2*wing_area) / (thrust_sl-72/1e3*alt))
        self.dt_dalt = 72/1e3 * ((0.5*rho*speed**2*wing_area*thrust_c) /\
                       (thrust_sl-72/1e3*alt)**2)

    def apply_dGdp(self, arguments):
        """ assign throttle directional derivatives """

        dpvec = self.vec['dp']
        dgvec = self.vec['dg']

        dalt = dpvec('h')
        dthrust_c = dpvec('CT_tar')
        drho = dpvec('rho')
        dspeed = dpvec('v')
        dtau = dgvec('tau')

        dt_dalt = self.dt_dalt
        dt_dthrust_c = self.dt_dthrust_c
        dt_drho = self.dt_drho
        dt_dspeed = self.dt_dspeed

        if self.mode == 'fwd':
            dtau[:] = 0.0
            if self.get_id('h') in arguments:
                dtau[:] += (dt_dalt * dalt) * 1e3
            if self.get_id('CT_tar') in arguments:
                dtau[:] += (dt_dthrust_c * dthrust_c) * 1e-1
            if self.get_id('rho') in arguments:
                dtau[:] += dt_drho * drho
            if self.get_id('v') in arguments:
                dtau[:] += (dt_dspeed * dspeed) * 1e2
        if self.mode == 'rev':
            dalt[:] = 0.0
            dthrust_c[:] = 0.0
            drho[:] = 0.0
            dspeed[:] = 0.0
            if self.get_id('h') in arguments:
                dalt[:] += dt_dalt * dtau * 1e3
            if self.get_id('CT_tar') in arguments:
                dthrust_c[:] += dt_dthrust_c * dtau * 1e-1
            if self.get_id('rho') in arguments:
                drho[:] += dt_drho * dtau
            if self.get_id('v') in arguments:
                dspeed[:] += dt_dspeed * dtau * 1e2

class SysTauSurrogate(ExplicitSystem):
    """ compute the throttle setting from target CT by using existing
        engine data
    """

    def _declare(self):
        """ owned variables: tau (throttle setting)
            dependencies: h (altitude)
                          temp (temperature)
                          v (speed)
                          CT (coefficient of thrust)
                          rho (density of air)
                          S (wing area)
        """

        self.num_elem = self.kwargs['num_elem']
        num_pts = self.num_elem+1
        ind_pts = self.range(num_pts)

        self._declare_variable('tau', size=num_pts)
        self._declare_argument('h', indices=ind_pts)
        self._declare_argument('Temp', indices=ind_pts)
        self._declare_argument('v', indices=ind_pts)
        self._declare_argument('CT_tar', indices=ind_pts)
        self._declare_argument('rho', indices=ind_pts)
        self._declare_argument(['S', 0], indices=[0])

        self.build_surrogate('UHB.outputFLOPS')

    def build_surrogate(self, file_name):
        """ builds the surrogate model from the data stored in the file name
            given in the input arguments
        """

        data_file = open(file_name, 'r')

        for i, l in enumerate(data_file):
            pass

        file_len = i+1
        mach = numpy.zeros(file_len)
        altitude = numpy.zeros(file_len)
        power_code = numpy.zeros(file_len)
        thrust = numpy.zeros(file_len)
        drag = numpy.zeros(file_len)
        TSFC = numpy.zeros(file_len)
        i = 0

        data_file = open(file_name, 'r')

        for line in data_file:
            [mach[i], altitude[i], power_code[i], thrust[i], drag[i], fuel_burn,
             TSFC[i], Nox, area] = line.split()
            i += 1

        mach = [float(i) for i in mach]
        altitude = [float(i) for i in altitude]
        power_code = [float(i) for i in power_code]
        thrust = [float(i) for i in thrust]
        drag = [float(i) for i in drag]
        TSFC = [float(i) for i in TSFC]



#    def apply_G(self):
