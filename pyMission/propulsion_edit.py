"""
INTENDED FOR MISSION ANALYSIS USE

Propulsion module based on propulsion surrogate model
"""

# pylint: disable=E1101
from __future__ import division

import numpy as np
#import sys
import MBI, scipy.sparse

from openmdao.main.api import ImplicitComponent, Component
from openmdao.main.datatypes.api import Array, Float

def setup_prop_surr(surr_prop_file):
    raw = np.loadtxt(surr_prop_file+'_inputs.dat')
    [Thrust, TSFC] = np.loadtxt(surr_prop_file+'_outputs.dat')

    M_num,h_num,t_num = raw[:3].astype(int)
    M_surr = raw[3:3 + M_num]
    h_surr = raw[3 + M_num:3 + M_num + h_num]
    t_surr = raw[3 + M_num + h_num:]

    mbi_Thrust = np.zeros((M_num,h_num,t_num))
    mbi_TSFC = np.zeros((M_num, h_num, t_num))

    count = 0
    for i in xrange(M_num):
        for j in xrange(h_num):
            for k in xrange(t_num):
                mbi_Thrust[i][j][k] = Thrust[count]
                mbi_TSFC[i][j][k] = TSFC[count]
                count += 1

    Thrust_arr = MBI.MBI(mbi_Thrust, [M_surr, h_surr, t_surr],
                         [M_num, h_num, t_num], [4, 4, 4])
    TSFC_arr = MBI.MBI(mbi_TSFC, [M_surr, h_surr, t_surr],
                       [M_num, h_num, t_num], [4, 4, 4])

    nums = {
        'M': M_num,
        'h': h_num,
        't': t_num,
        }

    #return [Thrust_arr, TSFC_arr, nums] 
    return [Thrust_arr,TSFC_arr, nums]

class SysPropSurrogate(ImplicitComponent):
    """ Propulsion Tau surrogate model """

    def __init__(self,num_elem=10, num=None, Thrust=None):
        super(SysPropSurrogate,self).__init__()

        # Inputs
        self.add('M', Array(np.zeros((num_elem+1, )), iotype='in', desc='Mach Number'))
        self.add('h', Array(np.zeros((num_elem+1, )), iotype='in', desc='Altitude'))
        self.add('CT_tar', Array(np.zeros((num_elem+1, )), iotype='in', desc='Target Thrust'))
        self.add('rho', Array(np.zeros((num_elem+1, )), iotype='in', desc='Density'))
        self.add('v', Array(np.zeros((num_elem+1, )), iotype='in', desc='Velocity'))
        self.add('S', Float(0.0,iotype='in',desc='Wing Area'))

        # States
        self.add('tau',Array(np.zeros((num_elem+1, )), iotype='state',desc='Throttle Setting'))

        # Residuals
        self.add('tau_res', Array(np.zeros((num_elem+1, )), iotype='residual', desc='residual of throttle setting'))

        self.num = num
        self.Thrust_arr = Thrust
        #self.wing_area = S
        self.J_CT = [None for i in range(3)]
        self.CT = [None for i in range(num_elem+1)]        

    def evaluate(self):
        """ Calculate residual for surrogate """

        n_elem = len(self.tau)

        tau = self.tau
        Mach = self.M
        alt = self.h*1e3
        CT_tar = self.CT_tar*1e-1
        rho = self.rho
        speed = self.v*1e2
        wing_area = self.S*1e2
        tau_res = self.tau_res

        inputs = np.zeros((n_elem,3))
        inputs[:,0] = Mach
        inputs[:,1] = alt
        inputs[:,2] = tau

        CT_temp = self.Thrust_arr.evaluate(inputs)

        CT  = np.zeros(n_elem)
        
        for index in xrange(n_elem):
            CT[index] = CT_temp[index,0]

        CT[:] = CT[:]/(0.5 * rho[:] * speed[:]**2 * wing_area)

        tau_res[:] = CT - CT_tar

    def list_deriv_vars(self):
        """ Return lists of inputs and outputs where we defined derivatives """
        input_keys = ['tau', 'M', 'h','CT_tar', 'rho', 'v']
        output_keys = ['tau_res']
        return input_keys, output_keys

    def provideJ(self):
        """ Calculates and saves derivatives. (i.e Jacobian) """

        n_elem = len(self.tau)

        tau = self.tau
        Mach = self.M
        alt = self.h

        inputs = np.zeros((n_elem,3))
        inputs[:, 0] = Mach
        inputs[:, 1] = alt
        inputs[:, 2] = tau

        for index in xrange(3):
            self.J_CT[index] = self.Thrust_arr.evaluate(inputs,1+index,0)[:,0]

        CT_temp = self.Thrust_arr.evaluate(inputs)
        for index in xrange(n_elem):
            self.CT[index] = CT_temp[index,0]



    def apply_deriv(self, arg, result):
        """ Compute the derivatives of thrust wrt to Mach, Alt, rho, speed
            Forward Mode"""

        rho = self.rho
        speed = self.v*1e2
        wing_area = self.S*1e2

        dres = result['tau_res']

        if 'M' in arg:
            dMach = arg['M']
            dres[:] += (self.J_CT[0])/ (0.5*rho[:] * speed[:]**2 * wing_area) * dMach

        if 'h' in arg:
            dalt = arg['h']
            dres[:] += (self.J_CT[1])/ (0.5 * rho[:] * speed[:]**2 * wing_area) * dalt * 1e3

        if 'tau' in arg:
            dtau = arg['tau']
            dres[:] += self.J_CT[2]/ (0.5 * rho[:] * speed[:]**2 * wing_area) * dtau

        if 'CT_tar' in arg:
            dCT = arg['CT_tar']
            dres[:] -= dCT

        if 'rho' in arg:
            drho = arg['rho']
            dres[:] -= self.CT/ (0.5 * rho[:]**2 * speed[:]**2 * wing_area) * drho

        if 'v' in arg:
            dspeed = arg['v']
            dres[:] -= 2*self.CT/ (0.5 * rho[:] * speed[:]**3  * wing_area) * dspeed *1e2



    def apply_derivT(self, arg, result):
        """ Compute the derivatives of thrust wrt to Mach, Alt, rho, speed.
           Adjoint Mode"""

        rho = self.rho
        speed = self.v*1e2
        wing_area = self.S*1e2

        dres  =arg['tau_res']

        if 'M' in result:
            dMach = result['M']
            dres[:] += (self.J_CT[0])/ (0.5*rho[:] * speed[:]**2 * wing_area) * dMach

        if 'h' in result:
            dalt = result['h']
            dres[:] += (self.J_CT[1])/ (0.5 * rho[:] * speed[:]**2 * wing_area) * dalt * 1e3

        if 'tau' in result:
            dtau = result['tau']
            dres[:] += self.J_CT[2]/ (0.5 * rho[:] * speed[:]**2 * wing_area) * dtau

        if 'CT_tar' in result:
            dCT = result['CT_tar']
            dres[:] -= dCT

        if 'rho' in result:
            drho = result['rho']
            dres[:] -= self.CT/ (0.5 * rho[:]**2 * speed[:]**2 * wing_area) * drho
        
        if 'v' in result:
            dspeed = result['v']
            

            ### debugging
            path = "check.txt"
            f = open(path,'w')
            f.write(str(self.CT) +"\n" + "\n")
            f.write(str(2*self.CT) +"\n" + "\n")
            f.write(str(np.shape(rho)) +"\n")
            f.write(str(np.shape(speed)) +"\n")
            f.write(str(wing_area) +"\n")
            f.write(str(np.shape(dspeed)) +"\n")
            f.write(str(np.shape(dres)) +"\n")
            f.close()
            ### 

            dres[:] -= self.CT/(0.5*0.5 * rho[:] * speed[:]**3 * wing_area)*dspeed*1e2
            
            
        

class sysSFC(component):
    """ SFC model from propulsion surrogate"""

    def __init__(self,num_elem=10, num=None, SFC=None):
        super(SysSFC,self).__init__()

        # Inputs
        self.add('M', Array(np.zeros((num_elem+1, )), iotype='in', desc='Mach Number'))
        self.add('h', Array(np.zeros((num_elem+1, )), iotype='in', desc='Altitude'))
        self.add('tau', Array(np.zeros((num_elem+1, )), iotype='in', desc='Throttle setting'))
        self.add('rho', Array(np.zeros((num_elem+1, )), iotype='in', desc='Density'))
        self.add('v', Array(np.zeros((num_elem+1, )), iotype='in', desc='Velocity'))
        self.add('S', Float(0.0,iotype='in',desc='Wing Area')


        # Outputs 
        self.add('SFC', Array(np.zeros((num_elem+1,)), iotype='out', desc='SFC'))
        
        self.num = num
        self.TSFC_arr = SFC
        self.J_SFC = [None for i in range(3)]

    def execute(self):
        """ Calculates SFC from surrogate"""
        
        n_elem = len(self.tau)
        tau = self.tau
        Mach = self.M
        alt = self.h*1e3

        inputs = np.zeros((n_elem,3))
        inputs[:,0]=Mach
        inputs[:,1]=tau               
        inputs[:,2]=alt

        SFC_temp = self.TSFC_arr.evaluate(inputs)

        SFC = np.zeros(n_elem)
        for index in xrange(n_elem):
                 SFC[index] = SFC_temp[index,0]

    def list_deriv_vars(self):
                 """ Returns the lists of input and outs where we derivatives have been defined"""

        input_keys = ['M','h','tau']
        output_keys = ['SFC']
        return input_keys, output_keys


    def provideJ(self):
        """ Calculates and saves derivatives (Jacobians)"""

        n_elem = len(self.tau)
            
        Mach - self.M
        tau = self.tau
        alt = self.h*1e3
                 
        inputs = np.zeros(n_elem,3)
        inputs[:,0] = Mach
        inputs[:,1] = tau
        inputs[:,2] = alt

        for index in xrange(3):
        self.J_SFC[index] = self.TSFC_arr.evaluate(inputs,1+index,0)[:,0]
                
        SFC_temp = self.TSFC_arr.evaluate(inputs)
        for index in xrange(n_elem):
            self.SFC[index] = SFC_temp[index,0]



    def apply_deriv(self,arg,result):
        """ Compute the derivatives of SFC wrt to Mach, Alt, tau, rho and speed. Forward Mode """
                 

        #rho = self.rho
        #speed = self.v*1e2
        #wing_area = self.S*1e2

        dSFC = result['SFC']

        if 'M' in arg:
            dMach=arg['M']
            dSFC[:] += self.J_SFC[0]*dMach

        if 'tau' in arg:
            dtau = arg['tau']
            dSFC[:] += self.J_SFC[1]*dtau
            
        if 'h' in arg:
            dalt = arg['h']
            dSFC[:] += self.J_slf[2]*dalt*1e3



    def apply_derivT(self,arg,result):
        """ Compute the derivatives of SFC wrt to Mach, Alt, tau, rho and speed. Adjoint Mode """
                 

        #rho = self.rho
        #speed = self.v*1e2
        #wing_area = self.S*1e2

        dSFC = arg['SFC']

        if 'M' in result:
            dMach=result['M']
            dSFC[:] += self.J_SFC[0]*dMach

        if 'tau' in result:
            dtau = result['tau']
            dSFC[:] += self.J_SFC[1]*dtau
            
        if 'h' in result:
            dalt = result['h']
            dSFC[:] += self.J_slf[2]*dalt*1e3



